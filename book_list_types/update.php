<?php
/**
 * Created by PhpStorm.
 * User: mahsyaj
 * Date: 14/12/2018
 * Time: 09:13
 */

require_once "common_header.php";

$validMasterKey = isset($_PUT[$apiMasterKey]);

if (isset($_PUT[$id]) && isset($_PUT[$name]) && isset($_PUT[$image])) {
    $testing = $_PUT[$id] < 0;

    if ($testing) {
        $response_code = ($dbManager->update($_PUT[$id], $_PUT[$name], $_PUT[$image])) ? 200 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else {
    $response_code = 400;
}

http_response_code($response_code);
