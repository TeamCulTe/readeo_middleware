<?php
/**
 * Created by PhpStorm.
 * User: mahsyaj
 * Date: 14/12/2018
 * Time: 09:13
 */

require_once "common_header.php";

$validToken = isset($_GET[$token]) && isset($_GET[$idUser]) && $userDbManager->checkToken($_GET[$idUser], $_GET[$token]);
$validMasterKey = isset($_GET[$apiMasterKey]);

if (isset($_GET[$updateQuery])) {
    $response = $dbManager->queryUpdateFields();
} else if (isset($_GET[$idUser]) && isset($_GET[$idBookListType])) {
    $testing = $_GET[$idUser] < 0 && $_GET[$idBookListType] < 0;

    if ($testing) {
        $response = $dbManager->getBookList($_GET[$idUser], $_GET[$idBookListType]);
    } else if ($validToken) {
        $response = $dbManager->getBookList($_GET[$idUser], $_GET[$idBookListType]);
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_GET[$idUser])) {
    $testing = $_GET[$idUser] < 0;

    if ($testing) {
        $response = $dbManager->getUserBookLists($_GET[$idUser]);
    } else if ($validToken) {
        $response = $dbManager->getUserBookLists($_GET[$idUser]);
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_GET[$count])) {
    $response = $dbManager->count();
} else if (isset($_GET[$start]) && isset($_GET[$end])) {
    $response = $dbManager->queryAllPaginated($_GET[$start], $_GET[$end]);
} else {
    $response = $dbManager->queryAll();
}

$response_code = ($response != null) ? 200 : 404;

echo $response;
http_response_code($response_code);
