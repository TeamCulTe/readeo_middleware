<?php
/**
 * Created by PhpStorm.
 * User: mahsyaj
 * Date: 14/12/2018
 * Time: 09:14
 */

require_once "common_header.php";

$validToken = isset($_PUT[$token]) && isset($_PUT[$idUser]) && $userDbManager->checkToken($_PUT[$idUser], $_PUT[$token]);
$validMasterKey = isset($_PUT[$apiMasterKey]);

if (isset($_PUT[$idUser]) && isset($_PUT[$idBookListType]) && isset($_PUT[$idBook])) {
    $testing = $_PUT[$idUser] < 0 && $_PUT[$idBookListType] < 0 && $_PUT[$idBook] < 0;

    if ($testing) {
        $response_code = ($dbManager->delete($_PUT[$idUser], $_PUT[$idBookListType], $_PUT[$idBook])) ? 200 : 404;
    } else if ($validToken) {
        $response_code = ($dbManager->delete($_PUT[$idUser], $_PUT[$idBookListType], $_PUT[$idBook])) ? 200 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$idUser]) && isset($_PUT[$idBook])) {
    $testing = $_PUT[$idUser] < 0 && $_PUT[$idBook] < 0;

    if ($testing) {
        $response_code = ($dbManager->deleteUserBookListBook($_PUT[$idUser], $_PUT[$idBook])) ? 200 : 404;
    } else if ($validToken) {
        $response_code = ($dbManager->deleteUserBookListBook($_PUT[$idUser], $_PUT[$idBook])) ? 200 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$idUser])) {
    $testing = $_PUT[$idUser] < 0;

    if ($testing) {
        $response_code = ($dbManager->deleteUserBookLists($_PUT[$idUser])) ? 200 : 404;
    } else if ($validToken) {
        $response_code = ($dbManager->deleteUserBookLists($_PUT[$idUser])) ? 200 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$idBookListType])) {
    $testing = $_PUT[$idBookListType] < 0;

    if ($testing) {
        $response_code = ($dbManager->deleteTypedBookLists($_PUT[$idBookListType])) ? 200 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$idBook])) {
    $testing = $_PUT[$idBook] < 0;

    if ($testing) {
        $response_code = ($dbManager->deleteBookBookLists($_PUT[$idBook])) ? 200 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$test])) {
    $dbManager->deleteTestEntities();
    $response_code = 200;
} else {
    $response_code = 400;
}

http_response_code($response_code);
