<?php
/**
 * Created by PhpStorm.
 * User: mahsyaj
 * Date: 14/12/2018
 * Time: 09:13
 */

require_once "common_header.php";

$validMasterKey = isset($_POST[$apiMasterKey]);

if (isset($_POST[$id]) && isset($_POST[$name])) {
    $testing = $_POST[$id] < 0;

    if ($testing) {
        $response_code = ($dbManager->fullCreate($_POST[$id], $_POST[$name])) ? 201 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_POST[$name]) && $validMasterKey) {
    $createdId = $dbManager->create($_POST[$name]);

    if ($createdId != "") {
        $response_code = 201;
    } else {
        $response_code = 404;
    }

    echo $createdId;
} else {
    $response_code = 400;
}

http_response_code($response_code);
