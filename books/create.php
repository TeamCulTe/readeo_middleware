<?php
/**
 * Created by PhpStorm.
 * User: mahsyaj
 * Date: 14/12/2018
 * Time: 09:13
 */

require_once "common_header.php";

$validMasterKey = isset($_POST[$apiMasterKey]);

if (isset($_POST[$id]) && isset($_POST[$idCategory]) && isset($_POST[$title]) && isset($_POST[$cover]) && isset($_POST[$summary]) &&
    isset($_POST[$datePublished])) {
    $testing = $_POST[$id] < 0;

    if ($testing) {
        $response_code = ($dbManager->fullCreate($_POST[$id], $_POST[$idCategory], $_POST[$title], $_POST[$cover], $_POST[$summary],
            $_POST[$datePublished])) ? 201 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_POST[$idCategory]) && isset($_POST[$title]) && isset($_POST[$cover]) && isset($_POST[$summary]) &&
    isset($_POST[$datePublished]) && $validMasterKey) {
    $createdId = $dbManager->create($_POST[$idCategory], $_POST[$title], $_POST[$cover], $_POST[$summary],
        $_POST[$datePublished]);

    if ($createdId != "") {
        $response_code = 201;
    } else {
        $response_code = 404;
    }

    echo $createdId;
} else {
    $response_code = 400;
}

http_response_code($response_code);
