<?php
/**
 * Created by PhpStorm.
 * User: mahsyaj
 * Date: 14/12/2018
 * Time: 09:13
 */

require_once "common_header.php";

$validMasterKey = isset($_POST[$apiMasterKey]);
$validToken = isset($_POST[$token]) && isset($_POST[$idUser]) && $userDbManager->checkToken($_POST[$idUser], $_POST[$token]);

if (isset($_POST[$idUser]) && isset($_POST[$idBook]) && isset($_POST[$review]) && isset($_POST[$shared])) {
    $testing = $_POST[$idUser] < 0 && $_POST[$idBook] < 0;

    if ($testing) {
        $response_code = ($dbManager->create($_POST[$idUser], $_POST[$idBook], $_POST[$review], $_POST[$shared])) ? 201 : 404;
    } else if ($validToken) {
        $response_code = ($dbManager->create($_POST[$idUser], $_POST[$idBook], $_POST[$review], $_POST[$shared])) ? 201 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_POST[$idUser]) && isset($_POST[$idBook]) && isset($_POST[$review])) {
    $testing = $_POST[$idUser] < 0 && $_POST[$idBook] < 0;

    if ($testing) {
        $response_code = ($dbManager->create($_POST[$idUser], $_POST[$idBook], $_POST[$review])) ? 201 : 404;
    } else if ($validToken) {
        $response_code = ($dbManager->create($_POST[$idUser], $_POST[$idBook], $_POST[$review])) ? 201 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else {
    $response_code = 400;
}

http_response_code($response_code);
