<?php
/**
 * Created by PhpStorm.
 * User: mahsyaj
 * Date: 14/12/2018
 * Time: 09:13
 */

require_once "common_header.php";

$validToken = isset($_PUT[$token]) && isset($_PUT[$id]) && $userDbManager->checkToken($_PUT[$idUser], $_PUT[$token]);
$validMasterKey = isset($_PUT[$apiMasterKey]);

if (isset($_PUT[$id]) && isset($_PUT[$pseudo]) && isset($_PUT[$password]) && isset($_PUT[$email]) &&
    isset($_PUT[$idProfile]) && isset($_PUT[$idCity]) && isset($_PUT[$idCountry])) {
    $testing = $_PUT[$id] < 0;

    if ($testing) {
        $response_code = ($dbManager->update($_PUT[$id], $_PUT[$pseudo], $_PUT[$password], $_PUT[$email],
            $_PUT[$idProfile], $_PUT[$idCity], $_PUT[$idCountry])) ? 201 : 404;
    } else if ($validToken) {
        $response_code = ($dbManager->update($_PUT[$id], $_PUT[$pseudo], $_PUT[$password], $_PUT[$email],
            $_PUT[$idProfile], $_PUT[$idCity], $_PUT[$idCountry])) ? 201 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$id]) && isset($_PUT[$pseudo])) {
    $testing = $_PUT[$id] < 0;

    if ($testing) {
        $response_code = ($dbManager->updatePseudo($_PUT[$id], $_PUT[$pseudo])) ? 201 : 404;
    } else if ($validToken) {
        $response_code = ($dbManager->updatePseudo($_PUT[$id], $_PUT[$pseudo])) ? 201 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$id]) && isset($_PUT[$password])) {
    $testing = $_PUT[$id] < 0;

    if ($testing) {
        $response_code = ($dbManager->updatePassword($_PUT[$id], $_PUT[$password])) ? 201 : 404;
    } else if ($validToken) {
        $response_code = ($dbManager->updatePassword($_PUT[$id], $_PUT[$password])) ? 201 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$id]) && isset($_PUT[$email])) {
    $testing = $_PUT[$id] < 0;

    if ($testing) {
        $response_code = ($dbManager->updateEmail($_PUT[$id], $_PUT[$email])) ? 201 : 404;
    } else if ($validToken) {
        $response_code = ($dbManager->updateEmail($_PUT[$id], $_PUT[$email])) ? 201 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$id]) && isset($_PUT[$idCity])) {
    $testing = $_PUT[$id] < 0;

    if ($testing) {
        $response_code = ($dbManager->updateCity($_PUT[$id], $_PUT[$idCity])) ? 201 : 404;
    } else if ($validToken) {
        $response_code = ($dbManager->updateCity($_PUT[$id], $_PUT[$idCity])) ? 201 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$id]) && isset($_PUT[$idCountry])) {
    $testing = $_PUT[$id] < 0;

    if ($testing) {
        $response_code = ($dbManager->updateCountry($_PUT[$id], $_PUT[$idCountry])) ? 201 : 404;
    } else if ($validToken) {
        $response_code = ($dbManager->updateCountry($_PUT[$id], $_PUT[$idCountry])) ? 201 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
 } else {
    $response_code = 400;
}

http_response_code($response_code);
