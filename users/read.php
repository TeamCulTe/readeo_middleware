<?php
/**
 * Created by PhpStorm.
 * User: mahsyaj
 * Date: 14/12/2018
 * Time: 09:13
 */

require_once "common_header.php";

if (isset($_GET[$updateQuery])) {
    $response = $dbManager->queryUpdateFields();
} else if (isset($_GET[$email]) && isset($_GET[$publicMode])) {
    if ($_GET[$publicMode] == true) {
        $response = $dbManager->getPublicUserFromEmail($_GET[$email]);
    } else if ($_GET[$publicMode] == false) {
        $response = $dbManager->getUserFromEmail($_GET[$email]);
    }
} else if (isset($_GET[$above]) && isset($_GET[$id])) {
    $response = $dbManager->queryAbove(($_GET[$id]));
} else if (isset($_GET[$id])) {
    $response = $dbManager->getUser($_GET[$id]);
} else if (isset($_GET[$pseudo]) && isset($_GET[$publicMode])) {
    if ($_GET[$publicMode] == true) {
        $response = $dbManager->getPublicUserFromPseudo($_GET[$pseudo]);
    } else if ($_GET[$publicMode] == false) {
        $response = $dbManager->getUserFromPseudo($_GET[$pseudo]);
    }
} else if (isset($_GET[$new]) && isset($_GET[$update])) {
    $response = $dbManager->queryNewer(($_GET[$update]));
} else if (isset($_GET[$email]) && isset($_GET[$password])) {
    $response = $dbManager->getUserFromAuth($_GET[$email], $_GET[$password]);
} else if (isset($_GET[$pseudo])) {
    $response = $dbManager->getUserId($_GET[$pseudo]);
} else if (isset($_GET[$email])) {
    $response = $dbManager->getUserFromEmail($email);
} else if (isset($_GET[$count])) {
    $response = $dbManager->count();
} else if (isset($_GET[$start]) && isset($_GET[$end]) && isset($_GET[$publicMode])) {
    $response = $dbManager->queryAllPublicPaginated($_GET[$start], $_GET[$end]);
} else if (isset($_GET[$publicMode]) && $_GET[$publicMode] == true) {
    $response = $dbManager->queryAllPublic();
} else if (isset($_GET[$start]) && isset($_GET[$end])) {
    // $response = $dbManager->queryAllPaginated($_GET[$start], $_GET[$end]);
} else {
    // $response = $dbManager->queryAll();
}

$response_code = ($response != null) ? 200 : 404;

echo $response;
http_response_code($response_code);
