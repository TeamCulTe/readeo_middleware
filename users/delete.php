<?php
/**
 * Created by PhpStorm.
 * User: mahsyaj
 * Date: 14/12/2018
 * Time: 09:14
 */

require_once "common_header.php";

$validToken = isset($_PUT[$token]) && isset($_PUT[$id]) && $userDbManager->checkToken($_PUT[$idUser], $_PUT[$token]);

if (isset($_PUT[$id])) {
    $testing = $_PUT[$id] < 0;

    if ($testing) {
        $response_code = ($dbManager->delete($_PUT[$id])) ? 200 : 404;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$email]) && isset($_PUT[$password])) {
    $testing = $_PUT[$email] == "testemail@test.fr" && $_PUT[$password] == "testPassword";

    if ($testing) {
        $userData = $dbManager->getUserFromAuth($_PUT[$email], $_PUT[$password]);
        $response_code = ($profileDbManager->delete($userData[0][UserDbManager::FIELDS[4]]) &&
            $reviewDbManager->deleteUserReviews($userData[0][UserDbManager::FIELDS[0]]) &&
            $quoteDbManager->deleteUserQuotes($userData[0][UserDbManager::FIELDS[0]]) &&
            $bookListDbManager->deleteUserBookLists($userData[0][UserDbManager::FIELDS[0]]) &&
            $dbManager->deleteFromAuth($_PUT[$email], $_PUT[$password])) ? 200 : 404;
    } else if ($validToken) {
        $userData = $dbManager->getUserFromAuth($_PUT[$email], $_PUT[$password]);
        $response_code = ($profileDbManager->delete($userData[0][UserDbManager::FIELDS[4]]) &&
            $reviewDbManager->deleteUserReviews($userData[0][UserDbManager::FIELDS[0]]) &&
            $quoteDbManager->deleteUserQuotes($userData[0][UserDbManager::FIELDS[0]]) &&
            $bookListDbManager->deleteUserBookLists($userData[0][UserDbManager::FIELDS[0]]) &&
            $dbManager->deleteFromAuth($_PUT[$email], $_PUT[$password])) ? 200 : 404;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$test])) {
    $dbManager->deleteTestEntities();
    $response_code = 200;
} else {
    $response_code = 400;
}

http_response_code($response_code);
