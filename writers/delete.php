<?php
/**
 * Created by PhpStorm.
 * User: mahsyaj
 * Date: 14/12/2018
 * Time: 09:14
 */

require_once "common_header.php";

$validMasterKey = isset($_PUT[$apiMasterKey]);

if (isset($_PUT[$idAuthor]) && isset($_PUT[$idBook])) {
    $testing = $_PUT[$idAuthor] < 0 && $_PUT[$idBook] < 0;

    if ($testing) {
        $response_code = ($dbManager->delete($_PUT[$idAuthor], $_PUT[$idBook])) ? 200 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$test])) {
    $dbManager->deleteTestEntities();
    $response_code = 200;
} else {
    $response_code = 400;
}

http_response_code($response_code);
