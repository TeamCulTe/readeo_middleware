<?php
/**
 * Created by PhpStorm.
 * User: mahsyaj
 * Date: 14/12/2018
 * Time: 09:13
 */

require_once "common_header.php";

$validMasterKey = isset($_POST[$apiMasterKey]);

if (isset($_POST[$idAuthor]) && isset($_POST[$idBook])) {
    $testing = $_POST[$idAuthor] < 0 && $_POST[$idBook] < 0;

    if ($testing) {
        $response_code = ($dbManager->create($_POST[$idAuthor], $_POST[$idBook])) ? 201 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else {
    $response_code = 400;
}

http_response_code($response_code);
