<?php
/**
 * Created by PhpStorm.
 * User: mahsyaj
 * Date: 14/12/2018
 * Time: 09:13
 */

require_once "common_header.php";

$validMasterKey = isset($_PUT[$apiMasterKey]);

if (isset($_PUT[$id])) {
    $testing = $_PUT[$id] < 0;

    if ($testing) {
        $response_code = ($dbManager->restoreSoftDeleted($_PUT[$id])) ? 200 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$idUser])) {
    $testing = $_PUT[$idUser] < 0;

    if ($testing) {
        $response_code = ($dbManager->restoreSoftDeletedUserQuotes($_PUT[$idUser])) ? 200 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else if (isset($_PUT[$idBook]) && ($_PUT[$idBook] < 0 || isset($_PUT[$apiMasterKey]))) {
    $testing = $_PUT[$idBook] < 0;

    if ($testing) {
        $response_code = ($dbManager->restoreSoftDeletedBookQuotes($_PUT[$idBook])) ? 200 : 404;
    } else if ($validMasterKey) {
        // TODO : Check the master key when implemented.
        $response_code = 403;
    } else {
        $response_code = 403;
    }
} else {
    $response_code = 400;
}

http_response_code($response_code);
